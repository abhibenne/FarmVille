# FarmVille (OnGoing)

An application bent on helping farmers perform at their best. Data from their soil, region, climate and other parameters like crop rotation, crop prices are taken into account to suggest the farmer the best crops suitable for the particular time and conditions. Insect, rodent and animal infestations in the area can be geolocated and the neighboring farmers can be alerted. Selling the crop is made easier by calculating and suggested the path for the maximum profit.
